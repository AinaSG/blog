---
title: "About Me"
date: 2021-03-29T05:01:42+02:00
draft: false
---

Aina Soler Gàlvez
======

#### Multidisciplinary developer. Mainly experience in Backends, specially for gameplay. 


Education
---------
**Informatics Engineering Degree - Univesitat Politècnica de Catalunya UPC-BarcelonaTech** 

- Speciallization in networks and operatin systems. 
- Degree thesis on distributed sensor nodes. (No2 Pollution - CAPTOR project)


**MOOCs** (ongoing)

- Fo Fill...

Experience
---------
**Backend developer - King** (2018-present, Barcelona, Spain)

- Backend programing for mobile games (Candy Crush, Pyramid Solitarie, Farm Heroes...) Including gameplay and operational code.
- Development of tooling for design and OTA content distribution.

**Fullstack developer - Inteligent Data Services** (2017-2018, Barcelona, Spain)

- Backend and some frontent for geographical behaviour data anatysis.

**Research Assistant, Frontent and Backend Developer, Hardware developer - UPC-BarcelonaTech** (2016-2017, Barcelona, Spain)

- Design and program NO2 sensor nodes in a distributed network.
- Develop a platform co control, receive and analise the data from those sensors.
- Parrt of European Union's CAPTOR project in collaboration with Ecologistas en Accion and Legambiente.

Other Experience
---------

**to fill**


Skills
------
**Programming:** Java, Python, C++, C, GDscript, ,Web Languages (Javascript, HTML...), C#, Lua, Earlang, ... (In order of experience)

**Languages:** Spanish (Native), Catalan (Native), English (Proficient)


Projects
--------
**[Check my projects in the project page!](http://project.page.url.lol)**